export declare enum VariableType {
    unknown = 0,
    object = 1,
    array = 2,
    number = 3,
    string = 4,
    boolean = 5,
    undefined = 6,
    null = 7,
    function = 8,
    date = 9,
    regExp = 10,
    symbol = 11,
}


export function getVariableType(variable: any): VariableType;
