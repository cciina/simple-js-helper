export default function randomStr(len: number, excludeGot?: boolean, seeds?: string[]): string;
