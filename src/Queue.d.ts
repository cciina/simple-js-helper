declare class Queue<T> {
    public get count(): number;

    public push(item: T): this;

    public pop(): T;

    public top(): T;

    public isHas(item: T): boolean;

    public toArray(): T[];

    public destroy(): void;
}

export default Queue;
