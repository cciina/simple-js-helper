export enum UserGender {
    unknown = 0,
    male = 1,
    female = 2,
}

export interface IGetOpenIdRes {
    "city": string;
    "country": string;
    "errcode": number;
    "errmsg": string | null;
    "headimgurl": string;
    "nickname": string;
    "openid": string;
    "privilege": [];
    "province": string;
    "sex": UserGender;
    "unionid": string | null;
}


export interface ParamBase_WxJsSDK {
    success?(res: unknown): void;

    fail?(reason: unknown): void;

    cancel?(res: unknown): void;

    complete?(res: unknown): void;
}

type numberL90to90 = number;
type numberL180to180 = number;
type number1to28 = number;

export interface IOpenLocationParam_WxJsSDK extends ParamBase_WxJsSDK {
    latitude: numberL90to90; // 纬度，浮点数，范围为90 ~ -90
    longitude: numberL180to180; // 经度，浮点数，范围为180 ~ -180。
    name: string; // 位置名
    address: string; // 地址详情说明
    scale?: number1to28; // 地图缩放级别,整型值,范围从1~28。默认为最大
    infoUrl?: string; // 在查看位置界面底部显示的超链接,可点击跳转
}

export enum GetLocationType_WxJsSDK {
    wgs84 = 'wgs84',
    gcj02 = 'gcj02',
}

type speedMetrePerSecond = number;

export interface IGetLocationRes_WxJsSDK {
    latitude: numberL90to90;
    longitude: numberL180to180;
    speed: speedMetrePerSecond;
    accuracy: any;
}

export enum NeedResultScanQRCode_WxJsSDK {
    processByWx = 0,
    returnIt = 1,
}

export enum ScanTypeScanQRCode_WxJsSDK {
    qrCode = 'qrCode',
    barCode = 'barCode',
}

export interface ScanQRCodeParam_WxJsSDK extends ParamBase_WxJsSDK {
    needResult?: NeedResultScanQRCode_WxJsSDK;
    scanType?: ScanTypeScanQRCode_WxJsSDK;
}

export enum ChooseImageSizeType_WxJsSDK {
    original = 'original',
    compressed = 'compressed',
}

export enum ChooseImageSourceType_WxJsSDK {
    album = 'album',
    camera = 'camera',
}

export interface ChooseImageParam_WxJsSDK extends ParamBase_WxJsSDK {
    count?: number;
    sizeType?: ChooseImageSizeType_WxJsSDK[];
    sourceType?: ChooseImageSourceType_WxJsSDK[];
}

export enum UploadImageShowProgress_WxJsSDK {
    yes = 1,
    no = 2,
}

export interface UploadImageParam_WxJsSDK extends ParamBase_WxJsSDK {
    localId: string;
    isShowProgressTips?: UploadImageShowProgress_WxJsSDK;
}
