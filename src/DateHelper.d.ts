export type AllowDate = Date | string | number;

export function format2Date(date: AllowDate): string;

export default class DateHelper {
    public static new(date: AllowDate, isSeconds ?: boolean): Date;

    public static toDate(date: AllowDate, isSecond?: boolean, dateSplitChar?: string): string;

    public static toTime(date: AllowDate, isSecond?: boolean): string;

    public static toDateTime(date: AllowDate, isSecond?: boolean, dateSplitChar?: string): string;

    public static toISO(date: AllowDate, isSecond?: boolean): string;

    public static toTimeStamp(date: AllowDate, isSecond?: boolean, returnSecond?: boolean): number;
}
