export interface IMobilePartialShowRes {
    mobile: string;
    source: string;
}

export default function mobilePartialShow(mobile: string): IMobilePartialShowRes;
