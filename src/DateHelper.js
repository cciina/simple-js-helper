function pad(n) {
    return 9 < n ? n : `0${n}`;
}

export default class DateHelper {
    static new(date, isSeconds = false) {
        if (date instanceof Date) {
            return new Date(date);
        } else {
            switch (typeof date) {
                case "number":
                    if (isSeconds) {
                        date = 1000 * date;
                    }
                    return new Date(date);
                case "string":
                    return new Date(date.replace(/-/g, '/'));
                default:
                    return null;
            }
        }
    }

    static toDate(date, isSecond = false, dateSplitChar = '-') {
        date = this.new(date, isSecond);
        return `${date.getFullYear()}${dateSplitChar}${pad(1 + date.getMonth())}${dateSplitChar}${pad(date.getDate())}`;
    }

    static toTime(date, isSecond = false) {
        date = this.new(date, isSecond);
        return `${pad(date.getHours())}:${pad(date.getMinutes())}:${pad(date.getSeconds())}`;
    }

    static toDateTime(date, isSecond = false, dateSplitChar = '-') {
        date = this.new(date, isSecond);
        return `${DateHelper.toDate(date, isSecond, dateSplitChar)} ${DateHelper.toTime(date)}`;
    }

    static toISO(date, isSecond = false) {
        return this.new(date, isSecond).toISOString();
    }

    static toTimeStamp(date, isSecond = false, returnSecond = false) {
        date = this.new(date, isSecond);
        date = date.getTime();
        return returnSecond ? Math.floor(date / 1000) : date;
    }
}
