import {IPinYinDictFirstLetter} from "./firstLetterDict";

interface IStringKeyValue {
    [propName: string]: string
}

interface IDict {
    firstletter?: IPinYinDictFirstLetter
    notone?: any
    py2hz?: any
    withtone?: any
}

interface IInitUtilParam {
    firstLetterDict?: IPinYinDictFirstLetter
    noToneDict?: any
    withToneDict?: any
    polyPhoneDict?: any
}


declare class PinYinUtil {
    constructor(param: IInitUtilParam);

    static getInstance(param: IInitUtilParam): PinYinUtil;

    parseDict();

    getPinyin(chinese: string, splitter?: string, withtone?: boolean, polyphone?: boolean): string;

    getFirstLetter(str: string, polyphone?: boolean): string;

    getHanzi(pinyin: string): string;

    removeTone(pinyin: string): string;

    getTone(pinyinWithoutTone: string): string;
}

export default PinYinUtil;
