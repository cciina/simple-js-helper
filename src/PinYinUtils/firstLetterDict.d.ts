export interface IPolyPhone {
    [propName: string]: string
}

export interface IPinYinDictFirstLetter {
    all: string;
    polyphone: IPolyPhone;
}

declare const firstLetterDict: IPinYinDictFirstLetter
export default firstLetterDict;
