import firstLetter from "./firstLetter.d";

export interface IPinYinUtils {
    firstLetter: typeof firstLetter;
}

declare const PinYinUtils: IPinYinUtils;

export default PinYinUtils;
