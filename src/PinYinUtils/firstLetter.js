/**
 * 转换为拼音首字母，大写字母
 */

import firstLetterDict from './firstLetterDict';

import PinYinUtil from './helper';

let helper;

export default function firstLetter(text) {

    if (!helper) {
        helper = PinYinUtil.getInstance({
            firstLetterDict,
        });
    }

    if (!!text && 0 < text.length) {
        return helper.getFirstLetter(text);
    } else {
        return '';
    }

}
