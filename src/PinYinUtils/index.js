import firstLetter from "./firstLetter";

const PinYinUtils = {
    firstLetter,
};
export default PinYinUtils;
