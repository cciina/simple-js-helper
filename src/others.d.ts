export interface IObject<T> {
    [prop: string]: T;
}

declare namespace otherHelpers {
    export const globalWin: Window | undefined;
    export const globalNode: IObject<any> | undefined;
    export const globalThis: Window | IObject<any> | undefined;
}

export default otherHelpers;

export function nothing2do(...args: unknown[]): void;

export const IsWKWebview: boolean;

export const EmptyArray: any[];
