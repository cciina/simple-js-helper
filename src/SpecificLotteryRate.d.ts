export interface SpecificLotteryRateItem<T> {
    rate: number;
    data: T;
}

declare class SpecificLotteryRate<T> {
    constructor(list: SpecificLotteryRateItem<T>[]);

    public getOne(): T;
}

export default SpecificLotteryRate;
