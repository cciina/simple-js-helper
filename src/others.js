import autoDisposeWhenUnload from "./autoDisposeWhenUnload";

let globalWin;
let globalNode;
let globalThisV;

autoDisposeWhenUnload(function () {
    globalWin = null;
    globalNode = null;
    globalThisV = null;
});

const otherHelpers = {};

Object.defineProperties(otherHelpers, {
    globalWin: {
        get() {
            if (!globalWin) {
                if ('undefined' !== typeof window) {
                    globalWin = window;
                }
            }
            return globalWin;
        },
    },
    globalNode: {
        get() {
            if (!globalNode) {
                if ('undefined' !== typeof global) {
                    globalNode = global;
                }
            }
            return globalNode;
        },
    },
    globalThis: {
        get() {
            if (!globalThisV) {
                if ('undefined' !== typeof globalThis) {
                    globalThisV = globalThis;
                } else if ('undefined' !== typeof otherHelpers.globalWin) {
                    globalThisV = otherHelpers.globalWin;
                } else if ('undefined' !== typeof otherHelpers.globalNode) {
                    globalThisV = otherHelpers.globalNode;
                }
            }
            return globalThisV;
        },
    },
});

export default otherHelpers;


export function nothing2do(...args) {
    console.warn(...args);
}

export const IsWKWebview = true === otherHelpers.globalWin?.__wxjs_is_wkwebview;

export const EmptyArray = [];
Object.freeze(EmptyArray);

export const UNDEFINED = void 0;
