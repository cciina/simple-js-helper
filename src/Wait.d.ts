declare class Wait {
    public static milliSeconds(ms): Promise<void>;
}

export default Wait;
