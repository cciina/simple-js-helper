declare class InfiniteLoopGet<T> {
    public constructor(list?: T[]);

    public get(): T | undefined;

    public destroy(): void;
}

export default InfiniteLoopGet;
