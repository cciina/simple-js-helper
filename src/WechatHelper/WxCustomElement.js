const WxCustomElement = {
    wxMp: 'wx-open-launch-weapp',
    app: 'wx-open-launch-app',
    subscribe: 'wx-open-subscribe',
    audio: 'wx-open-audio',
};
Object.freeze(WxCustomElement);

export default WxCustomElement;
