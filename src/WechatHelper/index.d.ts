import {
    ChooseImageParam_WxJsSDK,
    GetLocationType_WxJsSDK,
    IGetLocationRes_WxJsSDK,
    IGetOpenIdRes,
    IOpenLocationParam_WxJsSDK,
    ScanQRCodeParam_WxJsSDK,
    UploadImageParam_WxJsSDK,
} from "../interface/IWechatHelper";
import {IBase64String} from "../interface/ICommon";
import {IObject} from "../others";
import WxCustomElement from "./WxCustomElement";

declare class WechatHelper {
    public static get JsSDKList(): string[];

    public static customElement: WxCustomElement[];

    public static goAuth(): void;

    public static getCode(): string;

    public static userInfo(): IGetOpenIdRes | null;

    public static waitSDKReady(retry?: number): Promise<IObject<Function>>;

    public static closeWindow_JsSDK(): Promise<void>;

    public static openLocation_JsSDK(param: IOpenLocationParam_WxJsSDK): Promise<void>;

    public static getLocation_JsSDK(type: GetLocationType_WxJsSDK): Promise<IGetLocationRes_WxJsSDK>;

    public static scanQRCode_JsSDK(param?: ScanQRCodeParam_WxJsSDK): Promise<string | undefined>;

    public static chooseImage_JsSDK(param?: ChooseImageParam_WxJsSDK): Promise<string[]>;

    public static uploadImage_JsSDK(param?: UploadImageParam_WxJsSDK): Promise<string>;

    public static getLocalImgData_JsSDK(localId: string): Promise<IBase64String>;
}

export default WechatHelper;
