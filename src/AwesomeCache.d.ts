export interface IAwesomeCacheItem<T> {
    expireSeconds: number;

    getCacheContent(): T;
}

declare class AwesomeCache<T> {
    constructor(config: IAwesomeCacheItem<T>);

    refresh(): void;

    get(forceUpdate?: boolean): T;

    destroy(): void;
}

export default AwesomeCache
