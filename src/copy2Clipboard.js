import otherHelpers from "./others";
import autoDisposeWhenUnload from "./autoDisposeWhenUnload";

const nativeWin = otherHelpers.globalWin;

let textarea = null;

export default function copy2Clipboard(content) {
    if (!!nativeWin) {
        if (!textarea) {
            textarea = nativeWin.document.createElement('textarea');
            textarea.style.position = 'fixed';
            textarea.style.top = '0';
            textarea.style.left = '0';
            textarea.style.opacity = '0';

            nativeWin.document.body.appendChild(textarea);
            autoDisposeWhenUnload(function () {
                nativeWin.document.body.removeChild(textarea);
                textarea = null;
            });
        }

        textarea.style.display = 'block';
        textarea.value = content;
        textarea.select();

        nativeWin.document.execCommand('copy');

        textarea.style.display = 'none';
    }
}
