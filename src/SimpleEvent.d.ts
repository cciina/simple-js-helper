export type EventCallback = (...args: any[]) => void;

declare class SimpleEvent {
    public addEventListener(eventName: string, callback: EventCallback): void;

    public removeEventListener(eventName: string, callback: EventCallback): void;

    public trigger(eventName: string, ...args: any[]): void;

    public static addEventListener(eventName: string, callback: EventCallback): void;

    public static removeEventListener(eventName: string, callback: EventCallback): void;

    public static trigger(eventName: string, ...args: any[]): void;
}

export default SimpleEvent;
