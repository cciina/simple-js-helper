const min = 8;
const max = 16;

const lowerChars = [];
for (let code = 65; code < 91; code++) {
    lowerChars.push(String.fromCharCode(code));
}
const upperChars = [];
for (let code = 97; code < 123; code++) {
    upperChars.push(String.fromCharCode(code));
}
const numbers = [];
for (let code = 48; code < 58; code++) {
    numbers.push(String.fromCharCode(code));
}

function getDefaultUserInitPsw() {
    const l = lowerChars.slice();
    const u = upperChars.slice();
    const n = numbers.slice();

    const joinR = [
        l.splice(~~(l.length * Math.random()))[0],
        u.splice(~~(u.length * Math.random()))[0],
        n.splice(~~(n.length * Math.random()))[0],
    ];

    const joinA = [l, u, n,];
    let count = min + (~~((max - min + 1) * Math.random())) - 3;
    for (; count > 0; count--) {
        const joinIndex = ~~(3 * Math.random());
        const joinItem = joinA[joinIndex];
        joinR.splice((~~(joinR.length * Math.random())), 0, joinItem.splice(~~(joinItem.length * Math.random()), 1)[0]);
    }

    return joinR.join('');
}

export default getDefaultUserInitPsw;
