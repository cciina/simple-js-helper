import {IObject} from "./others";

declare class URLHelper {
    public constructor(url?: string);

    public search: IObject<string>;
    public hash: string;
    public serverPath: string;

    public clone(): URLHelper;

    public toString(): string;

    public destroy(): void;

    public static get current(): URLHelper;

    public static from(url: string): URLHelper;
}

export default URLHelper;
