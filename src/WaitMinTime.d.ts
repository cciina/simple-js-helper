declare class WaitMinTime {
    public static endTime: number;

    public start(waitMs?: number): void;

    public end(): Promise<void>;

    public destroy(): void;
}

export default WaitMinTime;
