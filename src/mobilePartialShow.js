export default function mobilePartialShow(mobile) {
    if (!!mobile) {
        return {
            mobile: mobile.slice(0, 3) + '****' + mobile.slice(7),
            source: mobile,
        };
    } else {
        return {
            mobile: '-',
            source: '-',
        };
    }
}
