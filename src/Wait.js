import otherHelpers from "./others";

const nativeWin = otherHelpers.globalWin;

export default class Wait {
    async static milliSeconds(ms) {
        if (!!nativeWin) {
            await new Promise(r => nativeWin.setTimeout(r, ms));
        }
    }
}
