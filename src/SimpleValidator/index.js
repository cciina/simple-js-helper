import isPlateNumber from "./isPlateNumber";

const SimpleValidator = {
    isPlateNumber,
};

export default SimpleValidator;
