import IIP  from "./isPlateNumber.d";

declare namespace SimpleValidator{
    export const isPlateNumber: typeof IIP
}

export default SimpleValidator;
