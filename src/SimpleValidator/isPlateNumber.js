// 武警车牌
const armedPoliceCarLicenseRegExp =
    /^WJ[京津冀晋蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼川贵云渝藏陕甘青宁新]?[0-9a-zA-Z]{5}$/;

function isArmedPoliceCarLicense(license) {
    return armedPoliceCarLicenseRegExp.test(license);
}


// 军车
const armyCarLicenseRegExp = /^[A-Z]{2}[0-9]{5}$/;

function isArmyCarLicense(license) {
    return armyCarLicenseRegExp.test(license);
}


// 民用车与使馆车
const normalRegExp =
    /^([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领 A-Z][A-Z](([0-9]{5}[A-Z])|([A-Z]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领 A-Z][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9 挂学警港澳使领])$/;

function isNormalCarLicense(license) {
    return normalRegExp.test(license);
}


export default function isPlateNumber(license) {
    return isNormalCarLicense(license) ||
        isArmedPoliceCarLicense(license) ||
        isArmyCarLicense(license);
}
