const VariableType = {
    unknown: 0,
    object: 1,
    array: 2,
    number: 3,
    string: 4,
    boolean: 5,
    undefined: 6,
    null: 7,
    function: 8,
    date: 9,
    regExp: 10,
    symbol: 11,
}

Object.freeze(VariableType);

export {
    VariableType,
};

export default function getVarType(variable) {
    const t = Object.prototype.toString.call(variable);
    switch (t) {
        case '[object Object]':
            return VariableType.object;
        case '[object Array]':
            return VariableType.array;
        case '[object Number]':
            return VariableType.number;
        case '[object String]':
            return VariableType.string;
        case '[object Boolean]':
            return VariableType.boolean;
        case '[object Undefined]':
            return VariableType.undefined;
        case '[object Null]':
            return VariableType.null;
        case '[object Function]':
            return VariableType.function;
        case '[object Date]':
            return VariableType.date;
        case '[object RegExp]':
            return VariableType.regExp;
        case '[object Symbol]':
            return VariableType.symbol;
        default:
            return VariableType.unknown;
    }
}
