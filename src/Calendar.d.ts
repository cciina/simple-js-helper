declare class Calendar {
    /**
     * 返回农历y年一整年的总天数
     */
    public static lYearDays(year: number): number;

    /**
     * 返回农历y年闰月是哪个月；若y年没有闰月 则返回0 (0-12)
     */
    public static leapMonth(y: number): 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;

    /**
     * 返回农历y年闰月的天数 若该年没有闰月则返回0 (0、29、30)
     */
    public static leapDays(y: number): 0 | 29 | 30;

    /**
     * 返回农历y年m月（非闰月）的总天数，计算m为闰月时的天数请使用leapDays方法 (-1、29、30)
     */
    public static monthDays(y: number, m: number): -1 | 29 | 30;

    /**
     * 返回公历(!)y年m月的天数 (-1、28、29、30、31)
     */
    public static solarDays(y: number, m: number): -1 | 28 | 29 | 30 | 31;

    /**
     * 农历年份转换为干支纪年
     */
    public static toGanZhiYear(lYear: number): string;
}

export default Calendar;
