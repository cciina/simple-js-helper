export default function autoDisposeWhenUnload(dispose: Function, thisArg?: any): () => void;
