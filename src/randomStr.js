const defaultSeeds = [];

function getDefaultSeeds() {
    if (0 === defaultSeeds.length) {
        for (let code = 33; code < 127; code++) {
            defaultSeeds.push(String.fromCharCode(code));
        }
    }
    return defaultSeeds;
}

export default function randomStr(len, excludeGot = false, seeds) {
    let currentSeeds;
    if (!seeds) {
        currentSeeds = getDefaultSeeds();
    } else {
        currentSeeds = seeds;
    }
    let seedsCount = currentSeeds.length;

    let getChar;
    if (excludeGot) {
        currentSeeds = currentSeeds.slice();
        getChar = () => {
            const index = ~~(seedsCount * Math.random());
            seedsCount--;
            return currentSeeds.splice(index, 1)[0];
        };
    } else {
        getChar = () => {
            const index = ~~(seedsCount * Math.random());
            return currentSeeds[index];
        };
    }

    return Array.apply(null, {length: len,}).map(getChar).join('');
}
